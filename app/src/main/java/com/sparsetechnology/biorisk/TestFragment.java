package com.sparsetechnology.biorisk;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class TestFragment extends Fragment {
    private Utils.Questions test;
    private int qIndex = 0;
    private int total = 0;

    private TextView questionTxt;

    private OnFragmentInteractionListener mListener;

    public TestFragment() {
    }

    public void setTest(Utils.Questions test) {
        this.test = test;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test, container, false);

        questionTxt = view.findViewById(R.id.test_question);

        Button yesBtn = view.findViewById(R.id.test_question_btn_yes);
        yesBtn.setText(test.qAnswers.get(0));

        Button noBtn = view.findViewById(R.id.test_question_btn_no);
        noBtn.setText(test.qAnswers.get(1));

        Button sometimesBtn = view.findViewById(R.id.test_question_btn_sometimes);
        sometimesBtn.setText(test.qAnswers.get(2));

        yesBtn.setOnClickListener(onClickListener);
        noBtn.setOnClickListener(onClickListener);
        sometimesBtn.setOnClickListener(onClickListener);

        return view;
    }

    private void setNextQuestionTxt() {
        questionTxt.setText(Html.fromHtml(String.format("<b>%s/%s</b><br>%s", qIndex + 1, test.qList.size(), test.qList.get(qIndex))));
        qIndex += 1;
    }

    @Override
    public void onResume() {
        super.onResume();
        setNextQuestionTxt();
    }

    private final Button.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.test_question_btn_yes:
                    total += 2;
                    break;
                case R.id.test_question_btn_no:
                    total += 0;
                    break;
                case R.id.test_question_btn_sometimes:
                    total += 1;
                    break;
            }

            if (qIndex >= test.qList.size()) {
                showEndTest(v.getContext());
                return;
            }

            setNextQuestionTxt();
        }
    };

    private void showEndTest(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Test End");
        builder.setMessage(String.format("Thanks, you got %s out of %s.", total, test.qList.size() * 2));
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialog.dismiss();
                dialog = null;

                if (mListener != null)
                    mListener.onEndTest();
            }
        });

        builder.create().show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onEndTest();
    }
}
