package com.sparsetechnology.biorisk;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements StartTestFragment.OnFragmentInteractionListener, TestFragment.OnFragmentInteractionListener {
    @BindView(R.id.main_container)
    public FrameLayout container;

    @BindView(R.id.main_title)
    TextView txtTitle;

    private Utils.Test test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Timber.plant(new Timber.DebugTree());

        txtTitle.setText(Html.fromHtml("<strong>BIO</strong>RISK"));
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            test = Utils.createTestFromJson(LoadData("questions.json"));

            StringBuilder langs = new StringBuilder();
            for (int i = 0; i < test.langs.size(); i++)
                langs.append(test.langs.get(i)).append(",");
            langs.deleteCharAt(langs.length() - 1);

            // Set start fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.main_container, StartTestFragment.newInstance(langs.toString())).commit();
        } catch (JSONException e) {
            Timber.e(e);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.main_container);
        if (f instanceof TestFragment) {
            onResume();
        } else if (f instanceof StartTestFragment) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Exit");
            builder.setMessage("Do you want to exit the application?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });

            builder.create().show();
        }
    }

    public String LoadData(String inFile) {
        String tContents = "";

        try {
            InputStream stream = getAssets().open(inFile);

            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            Timber.e(e, "LoadData");
        }

        if (tContents.isEmpty())
            Timber.w("Not read questions.json or file is empty.");
        else
            Timber.i("Read questions.json file.");

        return tContents;
    }

    @Override
    public void onStartWithLang(String lang) {
        TestFragment testFragment = new TestFragment();
        for (Utils.Questions questions : test.questions) {
            if (questions.lang.equals(lang)) {
                testFragment.setTest(questions);
                break;
            }
        }

        // Set start fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_container, testFragment).commit();
    }

    @Override
    public void onEndTest() {
        onResume();
    }
}
