package com.sparsetechnology.biorisk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class Utils {
    public static class Questions {
        public String lang = "";
        public ArrayList<String> qList = new ArrayList<>();
        public ArrayList<String> qAnswers = new ArrayList<>();
    }

    public static class Test {
        public ArrayList<String> langs = new ArrayList<>();
        public ArrayList<Questions> questions = new ArrayList<>();
    }

    public static Test createTestFromJson(String json) throws JSONException {
        Test test = new Test();

        JSONObject object = new JSONObject(json);

        Iterator keysIterator = object.keys();
        ArrayList<String> keysList = new ArrayList<String>();
        while (keysIterator.hasNext())
            keysList.add((String) keysIterator.next());

        test.langs = keysList;

        for (int item = 0; item < test.langs.size(); item++) {
            JSONObject o = object.getJSONObject(test.langs.get(item));
            JSONArray jArrayQ = o.getJSONArray("questions");
            JSONArray jArrayA = o.getJSONArray("answers");

            ArrayList<String> arrayQ = new ArrayList<>();
            ArrayList<String> arrayA = new ArrayList<>();

            for (int i1 = 0; i1 < jArrayQ.length(); i1++)
                arrayQ.add(jArrayQ.getString(i1));

            for (int i1 = 0; i1 < jArrayA.length(); i1++)
                arrayA.add(jArrayA.getString(i1));

            Questions questions = new Questions();
            questions.lang = test.langs.get(item);
            questions.qList = arrayQ;
            questions.qAnswers = arrayA;

            test.questions.add(questions);
        }

        return test;
    }
}
