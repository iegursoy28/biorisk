package com.sparsetechnology.biorisk;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

public class StartTestFragment extends Fragment {
    private static final String ARG_LANGS = "arg_langs";

    private String argLangs;

    private OnFragmentInteractionListener mListener;

    private AppCompatSpinner spinner;

    public StartTestFragment() {
    }

    public static StartTestFragment newInstance(String langs) {
        StartTestFragment fragment = new StartTestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_LANGS, langs);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            argLangs = getArguments().getString(ARG_LANGS);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_test, container, false);

        spinner = view.findViewById(R.id.start_select_lang);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(inflater.getContext(), android.R.layout.simple_spinner_item, argLangs.split(","));
        spinner.setAdapter(adapter);

        Button button = view.findViewById(R.id.start_button);
        button.setOnClickListener(onClickListener);

        return view;
    }

    private Button.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null)
                mListener.onStartWithLang(spinner.getSelectedItem().toString());
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onStartWithLang(String lang);
    }
}
